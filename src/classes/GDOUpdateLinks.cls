public class GDOUpdateLinks {
    
    @future
    public static void updateSetupLinks(String Member_Portal_Base_URL) {
        
        
        PagesApi__Theme__c th =[SELECT PagesApi__Supernav_HTML_Position_3__c FROM PagesApi__Theme__c WHERE Name = 'FON - LIGHTNING COMMUNITY SITE: MEMBERNATION THEME'];
        th.PagesApi__Supernav_HTML_Position_3__c = th.PagesApi__Supernav_HTML_Position_3__c.replace('https://us-tdm-tso-15eb63ff4c6-1626e3c741a.force.com', Member_Portal_Base_URL);
        update th;
        
        
        PagesApi__Form__c frm1 = [SELECT Id FROM PagesApi__Form__c WHERE Name = 'FON-CEU credit Submission'];
        LTE__Menu_Item__c menuItm1 = [SELECT LTE__URL__c FROM LTE__Menu_Item__c WHERE Name = 'Self Report Credits' AND LTE__Site__r.Name = 'FON - Lightning Member Community'];
        menuItm1.LTE__URL__c = menuItm1.LTE__URL__c.replace('a0S6A000002HeN3',frm1.Id);
        update menuItm1;
        
        PagesApi__Form__c frm2 = [SELECT Id FROM PagesApi__Form__c WHERE Name = 'FON-Company Information'];
        LTE__Menu_Item__c menuItm2 = [SELECT LTE__URL__c FROM LTE__Menu_Item__c WHERE Name = 'Update Company Information' AND LTE__Site__r.Name = 'FON - Lightning Member Community'];
        menuItm2.LTE__URL__c = menuItm2.LTE__URL__c.replace('a0S6A000001F2YtUAK',frm2.Id);
        update menuItm2;
        
        PagesApi__Form__c frm3 = [SELECT Id FROM PagesApi__Form__c WHERE Name = 'FON-Organization Contact Management'];
        LTE__Menu_Item__c menuItm3 = [SELECT LTE__URL__c FROM LTE__Menu_Item__c WHERE Name = 'Update Company Contacts' AND LTE__Site__r.Name = 'FON - Lightning Member Community'];
        menuItm3.LTE__URL__c = menuItm3.LTE__URL__c.replace('a0S6A000001Ftr3',frm3.Id);
        update menuItm3;
        
        OrderApi__Catalog__c cata = [SELECT Id FROM OrderApi__Catalog__c WHERE Name = 'FON-Certifications and Accreditations'];
        LTE__Menu_Item__c menuItm4 = [SELECT LTE__URL__c FROM LTE__Menu_Item__c WHERE Name = 'Apply/Maintain Certifications Today!' AND LTE__Site__r.Name = 'FON - Lightning Member Community'];
        menuItm4.LTE__URL__c = menuItm4.LTE__URL__c.replace('a0s6A000001CwDSQA0',cata.Id);
        update menuItm4;
        
        OrderApi__Campaign_Page__c cp= [SELECT Id FROM OrderApi__Campaign_Page__c WHERE OrderApi__Name__c LIKE '%Donation Page'];
        LTE__Menu_Item__c menuItm5 = [SELECT LTE__URL__c FROM LTE__Menu_Item__c WHERE Name = 'Donate Today!' AND LTE__Site__r.Name = 'FON - Lightning Member Community'];
        menuItm5.LTE__URL__c = menuItm5.LTE__URL__c.replace('a0q6A000001cUNbQAM',cp.Id);
        update menuItm5;
        
        DRCTS__Directories__c dir1 = [SELECT Id FROM DRCTS__Directories__c WHERE Name = 'FON - Individual Member Directory'];
        LTE__Menu_Item__c menuItm6 = [SELECT LTE__URL__c FROM LTE__Menu_Item__c WHERE Name = 'Individual Member Directory' AND LTE__Site__r.Name = 'FON - Lightning Member Community'];
        menuItm6.LTE__URL__c = menuItm6.LTE__URL__c.replace('a2n6A000000pgoD',dir1.Id);
        update menuItm6;
        
        DRCTS__Directories__c dir2 = [SELECT Id FROM DRCTS__Directories__c WHERE Name = 'FON- Organization Member Directory'];
        LTE__Menu_Item__c menuItm7 = [SELECT LTE__URL__c FROM LTE__Menu_Item__c WHERE Name = 'Organizational Member Dirctory' AND LTE__Site__r.Name = 'FON - Lightning Member Community'];
        menuItm7.LTE__URL__c = menuItm7.LTE__URL__c.replace('a2n6A000000VvXp',dir2.Id);
        update menuItm7;
        
        joinapi__Join_Process__c[] jpLst = [SELECT Id, joinapi__Landing_Url__c FROM joinapi__Join_Process__c];
        for (joinapi__Join_Process__c jp : jpLst) {
            if (jp.joinapi__Landing_Url__c != null)
                jp.joinapi__Landing_Url__c = jp.joinapi__Landing_Url__c.replaceAll('https://us-tdm-tso-15eb63ff4c6-1626e3c741a.force.com',Member_Portal_Base_URL);
        }
        update jpLst;
        
        
        joinapi__Step__c js1 = [SELECT Id FROM joinapi__Step__c WHERE joinapi__Is_First_Step__c = true AND joinapi__Join_Process__r.joinapi__Join_Process_Display_Name__c = 'FON - Individual Membership Application'];
        joinapi__Join_Process__c jp1 = [SELECT joinapi__Landing_Url__c FROM joinapi__Join_Process__c WHERE joinapi__Join_Process_Display_Name__c = 'FON - Individual Membership Application'];
        jp1.joinapi__Landing_Url__c = jp1.joinapi__Landing_Url__c.replace('a206A000000R93aQAC',js1.Id);
        update jp1;
        
        LTE__Menu_Item__c menuItm8 = [SELECT LTE__URL__c FROM LTE__Menu_Item__c WHERE Name = 'Individual Memberships' AND LTE__Site__r.Name = 'FON - Lightning Member Community'];
        menuItm8.LTE__URL__c = jp1.joinapi__Landing_Url__c;
        update menuItm8;
        
        
        joinapi__Step__c js2 = [SELECT Id FROM joinapi__Step__c WHERE joinapi__Is_First_Step__c = true AND joinapi__Join_Process__r.joinapi__Join_Process_Display_Name__c = 'FON - Company Membership Application'];
        joinapi__Join_Process__c jp2 = [SELECT joinapi__Landing_Url__c FROM joinapi__Join_Process__c WHERE joinapi__Join_Process_Display_Name__c = 'FON - Company Membership Application'];
        jp2.joinapi__Landing_Url__c = jp2.joinapi__Landing_Url__c.replace('a206A000000RAGiQAO',js2.Id);
        jp2.joinapi__Landing_Url__c = jp2.joinapi__Landing_Url__c.replace('/s/','/');
        update jp2;
        
        LTE__Menu_Item__c menuItm9 = [SELECT LTE__URL__c FROM LTE__Menu_Item__c WHERE Name = 'Company Memberships' AND LTE__Site__r.Name = 'FON - Lightning Member Community'];
        menuItm9.LTE__URL__c = jp2.joinapi__Landing_Url__c;
        update menuItm9;
        
        
        joinapi__Step__c js3 = [SELECT Id FROM joinapi__Step__c WHERE joinapi__Is_First_Step__c = true AND joinapi__Join_Process__r.joinapi__Join_Process_Display_Name__c = 'Fon - Individual Membership(National, State & Local)'];
        joinapi__Join_Process__c jp3 = [SELECT joinapi__Landing_Url__c FROM joinapi__Join_Process__c WHERE joinapi__Join_Process_Display_Name__c = 'Fon - Individual Membership(National, State & Local)'];
        jp3.joinapi__Landing_Url__c = jp3.joinapi__Landing_Url__c.replace('a206A000000ZepCQAS',js3.Id);
        update jp3;
        
        LTE__Menu_Item__c menuItm10 = [SELECT LTE__URL__c FROM LTE__Menu_Item__c WHERE Name = 'Individual Membership(National, State & Local)' AND LTE__Site__r.Name = 'FON - Lightning Member Community'];
        menuItm10.LTE__URL__c = jp3.joinapi__Landing_Url__c;
        update menuItm10;
        
        
        PagesApi__Form__c frm4 = [SELECT Id FROM PagesApi__Form__c WHERE Name = 'FON-CEU credit Submission'];
        PagesApi__Menu_Item__c menuItm11 = [SELECT PagesApi__Custom_URL__c FROM PagesApi__Menu_Item__c WHERE Name = 'Self Report Credits' AND PagesApi__Menu__r.Name = 'INTERNATIONAL ASSOCIATION: SITE NAV'];
        menuItm11.PagesApi__Custom_URL__c = menuItm11.PagesApi__Custom_URL__c.replace('a0S6A000002HeN3',frm4.Id);
        update menuItm11;
        
        
        OrderApi__Catalog__c cat = [SELECT Id FROM OrderApi__Catalog__c WHERE Name = 'FON-Certifications and Accreditations'];
        PagesApi__Menu_Item__c menuItm12 = [SELECT PagesApi__Custom_URL__c FROM PagesApi__Menu_Item__c WHERE Name = 'Apply/Maintain Today!' AND PagesApi__Menu__r.Name = 'INTERNATIONAL ASSOCIATION: SITE NAV'];
        menuItm12.PagesApi__Custom_URL__c = menuItm12.PagesApi__Custom_URL__c.replace('a0s6A000001CwDSQA0',cat.Id);
        update menuItm12;
        
        
        PagesApi__Form__c frm5 = [SELECT Id FROM PagesApi__Form__c WHERE Name = 'FON-Company Information'];
        PagesApi__Menu_Item__c menuItm13 = [SELECT PagesApi__Custom_URL__c FROM PagesApi__Menu_Item__c WHERE Name = 'Update Company Profile' AND PagesApi__Menu__r.Name = 'INTERNATIONAL ASSOCIATION: SITE NAV'];
        menuItm13.PagesApi__Custom_URL__c = menuItm13.PagesApi__Custom_URL__c.replace('a0S6A000001F2YtUAK',frm5.Id);
        update menuItm13;
        
    }
}