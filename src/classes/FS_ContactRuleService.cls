public with sharing class FS_ContactRuleService {
    
    public static void beforeInsert(List<Contact> records){
        FS_ContactRuleServiceHelper.onBeforeInsert(records);
    }
    
    public static void beforeUpdate(Contact[] records, Map<id,sObject> oldMap){
       FS_ContactRuleServiceHelper.isDuplicateEmailOnUser((Contact[]) records, oldMap); //add error commented out : as quick fix for production data issue
    }
    
    
    public static void afterUpdate(Contact[] records,Map<id,sObject> oldMap){
        FS_ContactRuleServiceHelper.UpdateRelatedUserInfo((List<Contact>)records, (Map<Id,Contact>)oldMap);
    }
    
    
}