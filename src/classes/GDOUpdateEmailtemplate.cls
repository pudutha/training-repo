public class GDOUpdateEmailtemplate {
    
    @future
    public static void updateEmailtemplate(String Member_Portal_Base_URL) {
        
        EmailTemplate eTemp1 = [SELECT Body FROM EmailTemplate WHERE Name = 'FON-RECEIPT- email after posting'];
        eTemp1.Body  = eTemp1.Body.replace('https://us-tdm-tso-15eb63ff4c6-1626e3c741a.force.com', Member_Portal_Base_URL);
        update eTemp1;
        
        EmailTemplate eTemp2 = [SELECT Body FROM EmailTemplate WHERE Name = 'FON-RECEIPT-Refund Notification to the Customer for New Refund'];
        eTemp2.Body  = eTemp2.Body.replace('https://fonteva-solutions-developer-edition.na31.force.com', Member_Portal_Base_URL);
        update eTemp2;
        
        
    }
}