@isTest
public class GDOUpdateLinks_Test{
    
    static testMethod void GDOUpdateLinksTest() {
        // Test the process of swapping out all GDO values for a new org
        Domain masterDomain = [SELECT Id, Domain FROM Domain LIMIT 1];
        
        PagesApi__Site__c newSite1 = new PagesApi__Site__c(Name ='FON-International Association', PagesApi__Site_URL__c = 'https://' + masterDomain.Domain);
        insert newSite1;
        
        GDO_Settings__c gs = new GDO_Settings__c(Member_Portal_Base_URL__c = 'https://us-tdm-tso-15eb63ff4c6-1626e3c741a.force.com');
        insert gs;
        
        LTE__Site__c ls = new LTE__Site__c(Name = 'FON - Lightning Member Community', LTE__Site_URL__c  = 'https://' + masterDomain.Domain + '/LightningMemberPortal/s');
        insert ls;
        
        PagesApi__Theme__c th = new PagesApi__Theme__c(
        PagesApi__Supernav_HTML_Position_3__c = '<li><a href="https://us-tdm-tso-15eb63ff4c6-1626e3c741a.force.com/LightningMemberPortal/s#/profile" target="_blank" id="Store" style="color: #ffffff!important;"><b>My Portal</b></a></li>', 
        Name = 'FON - LIGHTNING COMMUNITY SITE: MEMBERNATION THEME');
        insert th;
        
        PagesApi__Form__c frm1 = new PagesApi__Form__c(Name = 'FON-CEU credit Submission'); 
        insert frm1;
        
        LTE__Menu_Item__c menuItm1 = new LTE__Menu_Item__c(
        Name = 'Self Report Credits', 
        LTE__Site__c = ls.id, 
        LTE__URL__c = 'https://us-tdm-tso-15eb63ff4c6-1626e3c741a.force.com/LightningMemberPortal/cpbase__form?ID=a0S6A000002HeN3');
        insert menuItm1;
        
        PagesApi__Form__c frm2 = new PagesApi__Form__c(Name = 'FON-Company Information'); 
        insert frm2;
        
        LTE__Menu_Item__c menuItm2 = new LTE__Menu_Item__c(
        Name = 'Update Company Information', 
        LTE__Site__c = ls.id, 
        LTE__URL__c = 'https://us-tdm-tso-15eb63ff4c6-1626e3c741a.force.com/LightningMemberPortal/cpbase__form?id=a0S6A000001F2YtUAK');
        insert menuItm2;
        
        PagesApi__Form__c frm3 = new PagesApi__Form__c(Name = 'FON-Organization Contact Management'); 
        insert frm3;
        
        LTE__Menu_Item__c menuItm3 = new LTE__Menu_Item__c(
        Name = 'Update Company Contacts', 
        LTE__Site__c = ls.id, 
        LTE__URL__c = 'https://us-tdm-tso-15eb63ff4c6-1626e3c741a.force.com/LightningMemberPortal/cpbase__form?id=a0S6A000001Ftr3');
        insert menuItm3;
        
        OrderApi__Catalog__c  cat = new OrderApi__Catalog__c(Name = 'FON-Certifications and Accreditations'); 
        insert cat;
        
        LTE__Menu_Item__c menuItm4 = new LTE__Menu_Item__c(
        Name = 'Apply/Maintain Certifications Today!', 
        LTE__Site__c = ls.id, 
        LTE__URL__c = 'https://us-tdm-tso-15eb63ff4c6-1626e3c741a.force.com/LightningMemberPortal/s/store#/store/browse/cat/a0s6A000001CwDSQA0/tiles');
        insert menuItm4;
        
        OrderApi__Campaign_Page__c cp = new OrderApi__Campaign_Page__c(OrderApi__Name__c = 'FON Donation Page'); 
        insert cp;
        
        LTE__Menu_Item__c menuItm5 = new LTE__Menu_Item__c(
        Name = 'Donate Today!', 
        LTE__Site__c = ls.id, 
        LTE__URL__c = 'https://us-tdm-tso-15eb63ff4c6-1626e3c741a.force.com/LightningMemberPortal/OrderApi__campaign?id=a0q6A000001cUNbQAM');
        insert menuItm5;
        
        DRCTS__Directories__c  dir1 = new DRCTS__Directories__c(Name = 'FON - Individual Member Directory'); 
        insert dir1;
        
        LTE__Menu_Item__c menuItm6 = new LTE__Menu_Item__c(
        Name = 'Individual Member Directory', 
        LTE__Site__c = ls.id, 
        LTE__URL__c = 'https://us-tdm-tso-15eb63ff4c6-1626e3c741a.force.com/LightningMemberPortal/s/searchdirectory?id=a2n6A000000pgoD');
        insert menuItm6;
        
        DRCTS__Directories__c  dir2 = new DRCTS__Directories__c(Name = 'FON- Organization Member Directory'); 
        insert dir2;
        
        LTE__Menu_Item__c menuItm7 = new LTE__Menu_Item__c(
        Name = 'Organizational Member Dirctory', 
        LTE__Site__c = ls.id, 
        LTE__URL__c = 'https://us-tdm-tso-15eb63ff4c6-1626e3c741a.force.com/LightningMemberPortal/s/searchdirectory?id=a2n6A000000VvXp');
        insert menuItm7;
        
        joinapi__Join_Process__c jp = new joinapi__Join_Process__c(
        joinapi__Join_Process_Display_Name__c = 'FON - Individual Membership Application', 
        joinapi__Landing_Url__c = 'https://us-tdm-tso-15eb63ff4c6-1626e3c741a.force.com/LightningMemberPortal/joinapi__membershiplist?id=a206A000000R93aQAC&order=1');
        insert jp;
        
        joinapi__Step__c js1 = new joinapi__Step__c(joinapi__Is_First_Step__c = true, joinapi__Join_Process__c = jp.id);
        insert js1;
        
        LTE__Menu_Item__c menuItm8 = new LTE__Menu_Item__c(
        Name = 'Individual Memberships', 
        LTE__Site__c = ls.id, 
        LTE__URL__c = 'https://us-tdm-tso-15eb63ff4c6-1626e3c741a.force.com/LightningMemberPortal/joinapi__membershiplist?id=a201U0000001oUBQAY');
        insert menuItm8;
        
        joinapi__Join_Process__c jp1 = new joinapi__Join_Process__c(
        joinapi__Join_Process_Display_Name__c = 'FON - Company Membership Application', 
        joinapi__Landing_Url__c = 'https://us-tdm-tso-15eb63ff4c6-1626e3c741a.force.com/LightningMemberPortal/joinapi__membershiplist?id=a206A000000RAGiQAO&order=1');
        insert jp1;
        
        joinapi__Step__c js2 = new joinapi__Step__c(joinapi__Is_First_Step__c = true, joinapi__Join_Process__c = jp1.id);
        insert js2;
        
        LTE__Menu_Item__c menuItm9 = new LTE__Menu_Item__c(
        Name = 'Company Memberships', 
        LTE__Site__c = ls.id, 
        LTE__URL__c = 'https://us-tdm-tso-15eb63ff4c6-1626e3c741a.force.com/LightningMemberPortal/joinapi__membershiplist?id=a201U0000001oULQAY&order=1');
        insert menuItm9;
        
        joinapi__Join_Process__c jp2 = new joinapi__Join_Process__c(
        joinapi__Join_Process_Display_Name__c = 'Fon - Individual Membership(National, State & Local)', 
        joinapi__Landing_Url__c = 'https://us-tdm-tso-15eb63ff4c6-1626e3c741a.force.com/LightningMemberPortal/joinapi__membershiplist?id=a206A000000ZepCQAS&order=1');
        insert jp2;
        
        joinapi__Step__c js3 = new joinapi__Step__c(joinapi__Is_First_Step__c = true, joinapi__Join_Process__c = jp2.id);
        insert js3;
        
        LTE__Menu_Item__c menuItm10 = new LTE__Menu_Item__c(
        Name = 'Individual Membership(National, State & Local)', 
        LTE__Site__c = ls.id, 
        LTE__URL__c = 'https://us-tdm-tso-15eb63ff4c6-1626e3c741a.force.com/LightningMemberPortal/joinapi__membershiplist?id=a201U0000001oULQAY');
        insert menuItm10;
        
        PagesApi__Menu__c mn = new PagesApi__Menu__c(Name = 'INTERNATIONAL ASSOCIATION: SITE NAV');
        insert mn;
        
        PagesApi__Menu_Item__c menuItm11 = new PagesApi__Menu_Item__c(
        Name = 'Self Report Credits', 
        PagesApi__Menu__c = mn.id, 
        PagesApi__Custom_URL__c = '/cpbase__form?ID=a0S6A000002HeN3');
        insert menuItm11;
        
        PagesApi__Menu_Item__c menuItm12 = new PagesApi__Menu_Item__c(
        Name = 'Apply/Maintain Today!', 
        PagesApi__Menu__c = mn.id, 
        PagesApi__Custom_URL__c = '/CPBase__store?site=a0d1U000000BGhPQAW&catalog=a0s6A000001CwDSQA0');
        insert menuItm12;
        
        PagesApi__Menu_Item__c menuItm13 = new PagesApi__Menu_Item__c(
        Name = 'Update Company Profile', 
        PagesApi__Menu__c = mn.id, 
        PagesApi__Custom_URL__c = 'https://us-tdm-tso-15eb63ff4c6-1626e3c741a.force.com/cpbase__form?id=a0S6A000001F2YtUAK&page=a0Z1U000002idYxUAI&site=a0d1U000000BGhPQAW');
        insert menuItm13;
        
        // "Hit the button"
        GDO_SetupButton_Ctrl setupPage = new GDO_SetupButton_Ctrl();
        Test.startTest();
        setupPage.runReplace();
        Test.stopTest();
        
        // Determine what the new Theme value SHOULD be
        PagesApi__Theme__c theme = [SELECT PagesApi__Supernav_HTML_Position_3__c FROM PagesApi__Theme__c WHERE Name = 'FON - LIGHTNING COMMUNITY SITE: MEMBERNATION THEME' LIMIT 1];
        String expectedURL = '<li><a href="https://' + masterDomain.Domain + '/LightningMemberPortal/s#/profile" target="_blank" id="Store" style="color: #ffffff!important;"><b>My Portal</b></a></li>';
        System.assertEquals(expectedURL, theme.PagesApi__Supernav_HTML_Position_3__c);
    }
}