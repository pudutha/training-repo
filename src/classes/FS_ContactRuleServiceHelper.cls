public class FS_ContactRuleServiceHelper{ 
    
    public static void onBeforeInsert(List<Contact> conList){     
        system.debug('inside test');            
        for(Contact oContact: conList){
            if(String.isBlank(oContact.OrderApi__Personal_Email__c) && !String.isBlank(oContact.Email)){
                oContact.OrderApi__Preferred_Email_Type__c= 'Personal';
                oContact.OrderApi__Personal_Email__c = oContact.Email;
            }
        }
    }
    public static void isDuplicateEmailOnUser(List<Contact> conList, Map<id, sObject> oldMap){ // email: *abc@mail.com
        Set<ID> contactIds = new Set<ID>();
        Set<String> emailIds = new Set<String>();
        
        Map<id, Contact> contact2ProcessforDuplicates = new map<id, Contact>();
        Map<Id,Contact> oldcMap =new Map<Id,Contact>();
        for(sObject sbj : oldMap.values())
        {
            Contact con =(Contact) sbj;
            oldcMap.put(con.Id, con);
        }
        SYSTEM.DEBUG('conList' + conList + ' oldMap  ' + oldMap + 'oldcMap ' + oldcMap);
        for(Contact con :conList){
            if(oldcMap.containsKey(con.id)){
                if(con.Email != oldcMap.get(con.id).Email){
                    if((!String.isBlank(con.Email) || !String.isBlank(con.OrderApi__Preferred_Email__c) )){ //eliminate all contacts having null emails
                        contactIds.add(con.id);
                        emailIds.add(con.Email);
                    }
                }
            }
        }
        Set<id> contactIdsToUpdate = new Set<id>();
        Map<String, User> usernameMap = new Map<String, User>(); 
        if(!emailIds.isEmpty()){
            //get such user which has same username email as we are trying to update from contact
            for(user u: [SELECT Id, Email, username,ContactId FROM USER WHERE username IN:emailIds]){
                usernameMap.put(u.username, u);
                
            }//email: *abc@mail.com [1]
            system.debug(usernameMap);        
            for(Contact oContact: conList){
                if(usernameMap.size()>0){
                    if(usernameMap.containsKey(oContact.Email)){
                        if(usernameMap.get(oContact.Email).contactId != oContact.Id){
                            // oContact.addError(System.Label.Duplicate_User_Name); //add error commented out : as quick fix for production data issue
                            contactIdsToUpdate.add(usernameMap.get(oContact.Email).contactId); //sud be removed if checking duplicate email on  after update
                        }else{contactIdsToUpdate.add(usernameMap.get(oContact.Email).contactId); }
                    }else contactIdsToUpdate.add(oContact.id);}else contactIdsToUpdate.add(oContact.id);}
            if(contactIdsToUpdate.size()>0 && !System.isFuture() && !System.isBatch()){
                updateUser(contactIdsToUpdate); 
            }
        }
    }
    
    @future
    public static void updateUser(Set<Id> contactIds){
        try
        {
            List<User> userList = new List<User>();
            Map<String,Contact> contactMap = new Map<String,Contact>();
            for(Contact con :[select id, email, firstname, lastname from contact where id IN:contactIds AND (email != NULL OR OrderApi__Personal_Email__c != NULL)])
            {
                contactMap.put(con.Id, con);                }
            for(user u: [SELECT Id, Email, FirstName, LastName, ContactId, profileid FROM USER WHERE ContactId IN: contactIds and ContactId != NULL  ]){
                if(contactMap.containsKey(u.contactId)) {
                    if(contactMap.get(u.ContactId).Email !=null) {
                        u.Email = contactMap.get(u.ContactId).Email;
                        u.Username = contactMap.get(u.ContactId).Email; 
                    } u.FirstName = contactMap.get(u.ContactId).FirstName; 
                    u.LastName = contactMap.get(u.ContactId).LastName; 
                    userList.add(u); 
                }                    
            }
            if(userList.size()>0){
                Database.update(userList,false);
            }
        }catch(DMLException e){
            //send send email
            String errorMsg =e.getMessage();
        }
    }
    
    /*update contact info to user*/
    
    public static void UpdateRelatedUserInfo(List<Contact> records, Map<Id, Contact> oldMap) {
        List<ContactToUserFieldMapping__c> fieldsMappedToUser =  ContactToUserFieldMapping__c.getAll().values();
        List<User> usersRelatedToContacts = [SELECT Id, ContactId FROM User WHERE ContactId IN :records];
        Map<Id, Contact> contactsMap = new Map<Id, Contact>(records);
        Set<Id> usersIdsWithUpdatedContacts = new Set<Id>();
        for (User user : usersRelatedToContacts) {
            Contact contact = contactsMap.get(user.ContactId);
            for (ContactToUserFieldMapping__c fieldMapping : fieldsMappedToUser) {
                if (fieldMapping.IsActive__c && !fieldMapping.Is_Formula_Field__c && contact.get(fieldMapping.ContactField__c) != oldMap.get(contact.Id).get(fieldMapping.ContactField__c)) {
                    usersIdsWithUpdatedContacts.add(user.Id);
                    break;
                }
            }
        }
        //UpdateUserInformationFuture(usersIdsWithUpdatedContacts);
        ID jobID = System.enqueueJob(new UpdateUserInformation(usersIdsWithUpdatedContacts));
    }
    
    class UpdateUserInformation implements Queueable {
        private Set<Id> userIds;
        
        public UpdateUserInformation(Set<Id> userIds) {
            this.userIds = userIds;
        }
        
        public void execute(QueueableContext context) {
            List<ContactToUserFieldMapping__c> fieldsMappedToUser =  ContactToUserFieldMapping__c.getAll().values();
            List<String> userFieldsNames = new List<String>();
            List<String> contactFieldsNames = new List<String>();
            Boolean activeMappingAvailable = false;
            for (ContactToUserFieldMapping__c fieldMapping : fieldsMappedToUser) {
                if (fieldMapping.IsActive__c && String.isNotBlank(fieldMapping.UserField__c)) {
                    userFieldsNames.add(fieldMapping.UserField__c);
                    contactFieldsNames.add('Contact.'+fieldMapping.ContactField__c);
                    activeMappingAvailable = true;
                }
            }
            
            if (!activeMappingAvailable) {
                return;
            }
            String query = 'SELECT Id,'+String.join(userFieldsNames, ',')+','+String.join(contactFieldsNames, ',')+' FROM User WHERE Id IN :userIds';
            system.debug('--framed query--'+query);
            List<User> usersToUpdate = Database.query(query);
            
            for (User user : usersToUpdate) {
                for (ContactToUserFieldMapping__c fieldMapping : fieldsMappedToUser) {
                    if (fieldMapping.IsActive__c) {
                        user.put(fieldMapping.UserField__c, user.getSObject('Contact').get(fieldMapping.ContactField__c));
                    }
                }
            }
            if (Test.isRunningTest()) {
                Profile testProfile = [SELECT Id 
                                       FROM profile
                                       WHERE Name = 'System Administrator' 
                                       LIMIT 1];
                String orgId = UserInfo.getOrganizationId();
                String dateString = 
                String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
                Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
                String uniqueName = orgId + dateString + randomInt;
                User testUser2 = new User(LastName = 'test user 2', 
                                          Username = uniqueName + '@test' + orgId + '.org',
                                          Email = uniqueName + '@test' + orgId + '.org', 
                                          Alias = 'te2322', 
                                          TimeZoneSidKey = 'GMT', 
                                          LocaleSidKey = 'en_GB', 
                                          EmailEncodingKey = 'ISO-8859-1', 
                                          ProfileId = testProfile.Id, 
                                          LanguageLocaleKey = 'en_US');
                List<User> allUsers = [Select id from User where UserName = :testUser2.Username];
                if (allUsers.size() > 0) {
                    testUser2.Username += '.tfs';
                }
                System.runAs(testUser2) {
                    update usersToUpdate;
                }
            } else {
                update usersToUpdate;
            }
            
        }
    }
    
}